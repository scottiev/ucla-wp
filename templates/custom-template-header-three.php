<header class="header">

  <div class="custom-template-hero-three">

    <img src="<?php if ( has_post_thumbnail() ) { echo $image; } ?>" class="custom-template-hero__image" alt="">

    <div class="custom-template-hero-three_title">
      <h1 class="ribbon"><?php the_title(); ?></h1>
    </div>

  </div>

</header>
